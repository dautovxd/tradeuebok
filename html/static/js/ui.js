(function ($) {
    'use strict';

    function getPreloader(text) {
        text = text || '';
        return $('<div class="preloader-block__inner">'
            + '<div class="preloader-block__inner_circle"></div>'
            + '<div class="preloader-block__inner_text">' + text + '</div>'
            + '</div>');
    }

    function deletePreloaders() {
        $('.preloader-block')
            .removeClass('preloader-block');
        $('.preloader-block__inner')
            .remove();
    }

    function initInput() {
        var $field = $('.form-field');

        $field.each(function () {
            var $that = $(this);
            var $input = $(this).find('input');
            var $textarea = $(this).find('textarea');

            $input.attr("spellcheck", "false");
            $textarea.attr("spellcheck", "false");

            $textarea.on('focus', function (event) {
                if ($(this).val() === '') {
                    $that
                        .removeClass('form-field--full form-ok');
                }

                event.preventDefault();
            }).on('blur', function (event) {
                if ($(this).val() !== '') {
                    $that.addClass('form-field--full form-field--full');
                }

                event.preventDefault();
            }).on('keypress', function (event) {
                var e = e || event;

                if (e.ctrlKey || e.altKey || e.metaKey) return;
                var chr = getChar(e);

                if (chr == null) return;

                switch ($that.find('label').html().toLowerCase()) {
                    case 'телефон':
                        if ((chr >= '0' && chr <= '9') || chr === '+' || chr === '-' || chr === ' ') {
                            break;
                        } else {
                            return false;
                        }
                }

                function getChar(event) {
                    if (event.which == null) {
                        if (event.keyCode < 32) return null;
                        return String.fromCharCode(event.keyCode);
                    }
                    if (event.which != 0 && event.charCode != 0) {
                        if (event.which < 32) return null;
                        return String.fromCharCode(event.which);
                    }
                    return null;
                }
            }).on('input', function (event) {
                var $currentInput = $(this);

                if ($that.hasClass('form-necessarily')) {
                    if ($currentInput.val() !== '') {
                        $that
                            .removeClass('form-error')
                            .addClass('form-ok form-ok__textarea');

                        $that.find('span.form-message').remove();
                        $that.find('span.form-text').css('padding-top', '');

                        $that.trigger('full');

                        switch ($that.find('label').html().toLowerCase()) {
                            case 'телефон':
                                if (!/^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]{7,13}$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that.append('<span class="form-message">Номер введён некорректно</span>');
                                }
                                event.preventDefault();
                                break;
                            case 'email':
                                if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that
                                        .append('<span class="form-message">Email введён некорректно</span>');
                                }
                                event.preventDefault();
                                break;
                            case 'имя':
                            case 'фамилия':
                                if (!/^[-A-Za-zА-Яа-яЁё ]+$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that
                                        .find('span.form-text').css('padding-top', '20px');
                                    $that
                                        .append('<span class="form-message">Некорректный ввод</span>');
                                }
                                event.preventDefault();
                                break;
                            case 'логин':
                                if (!/^[A-Za-z_0-9]+$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that
                                        .find('span.form-text').css('padding-top', '20px');
                                    $that
                                        .append('<span class="form-message">Некорректный ввод</span>');
                                }
                                event.preventDefault();
                                break;
                        }
                    } else {
                        $that.removeClass('form-ok')
                    }
                    event.preventDefault();
                } else {
                    if ($currentInput.val() !== '') {
                        $that
                            .removeClass('form-error')
                            .addClass('form-ok form-ok__textarea');

                        $that.find('span.form-message').remove();
                        $that.find('span.form-text').css('padding-top', '');

                        $that.trigger('full');

                        switch ($that.find('label').html().toLowerCase()) {
                            case 'имя':
                                if (!/^[-A-Za-zА-Яа-яЁё ]+$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that
                                        .append('<span class="form-message">Некорректный ввод</span>');
                                }
                                event.preventDefault();
                                break;
                        }
                    } else {
                        $that.removeClass('form-ok');
                    }
                }
            });

            $input.on('focus', function (event) {
                if ($(this).val() === '') {
                    $that
                        .removeClass('form-field--full form-ok');
                }

                event.preventDefault();
            }).on('blur', function (event) {
                if ($(this).val() !== '') {
                    $that.addClass('form-field--full');
                }

                event.preventDefault();
            }).on('keypress', function (event) {
                var e = e || event;

                if (e.ctrlKey || e.altKey || e.metaKey) return;
                var chr = getChar(e);

                if (chr == null) return;

                switch ($that.find('label').html().toLowerCase()) {
                    case 'телефон':
                        if ((chr >= '0' && chr <= '9') || chr === '+' || chr === '-' || chr === ' ') {
                            break;
                        } else {
                            return false;
                        }
                }

                function getChar(event) {
                    if (event.which == null) {
                        if (event.keyCode < 32) return null;
                        return String.fromCharCode(event.keyCode);
                    }
                    if (event.which != 0 && event.charCode != 0) {
                        if (event.which < 32) return null;
                        return String.fromCharCode(event.which);
                    }
                    return null;
                }
            }).on('input', function (event) {
                var $currentInput = $(this);

                if ($that.hasClass('form-necessarily')) {
                    if ($currentInput.val() !== '') {
                        $that
                            .removeClass('form-error')
                            .addClass('form-ok');

                        $that.find('span.form-message').remove();
                        $that.find('span.form-text').css('padding-top', '');

                        $that.trigger('full');

                        switch ($that.find('label').html().toLowerCase()) {
                            case 'телефон':
                                if (!/^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]{7,13}$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that.append('<span class="form-message">Номер введён некорректно</span>');
                                }
                                event.preventDefault();
                                break;
                            case 'email':
                                if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that
                                        .append('<span class="form-message">Email введён некорректно</span>');
                                }
                                event.preventDefault();
                                break;
                            case 'имя':
                            case 'фамилия':
                                if (!/^[-A-Za-zА-Яа-яЁё ]+$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that
                                        .find('span.form-text').css('padding-top', '20px');
                                    $that
                                        .append('<span class="form-message">Некорректный ввод</span>');
                                }
                                event.preventDefault();
                                break;
                            case 'логин':
                                if (!/^[A-Za-z_0-9]+$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that
                                        .find('span.form-text').css('padding-top', '20px');
                                    $that
                                        .append('<span class="form-message">Некорректный ввод</span>');
                                }
                                event.preventDefault();
                                break;
                        }
                    } else {
                        $that.removeClass('form-ok')
                    }
                    event.preventDefault();
                } else {
                    if ($currentInput.val() !== '') {
                        $that
                            .removeClass('form-error')
                            .addClass('form-ok');

                        $that.find('span.form-message').remove();
                        $that.find('span.form-text').css('padding-top', '');

                        $that.trigger('full');

                        switch ($that.find('label').html().toLowerCase()) {
                            case 'имя':
                                if (!/^[-A-Za-zА-Яа-яЁё ]+$/.test($currentInput.val())) {
                                    $that
                                        .removeClass('form-ok')
                                        .addClass('form-error')
                                        .trigger('blank')
                                        .find('span.form-message').remove();
                                    $that
                                        .append('<span class="form-message">Некорректный ввод</span>');
                                }
                                event.preventDefault();
                                break;
                        }
                    } else {
                        $that.removeClass('form-ok');
                    }
                }
            });
        });

        $('.checkbox label a').on('click', function (e) {
            e.stopPropagation()
        });

        $('.checkbox label').on('click', function () {
            $(this).siblings('input').prop("checked", !$(this).prev('input').prop("checked"));
            $(this).trigger('full');
            return false;
        })
    }

    function resetForms(options, init) {
        var $block = $(options.selector).slice(0, options.blocksNumber);

        $block.each(function (blockIndex) {
            if (options.initHide && options.initHide.indexOf(blockIndex) !== -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
            $(this).find('input').val('');
            $(this).find('.form-field')
                .removeClass('form-ok form-error form-field--full');
            $(this).find('.form-message')
                .remove();
            $(this).find('button')
                .removeClass('btn-red btn-green')
                .addClass('btn-disabled');
        });

        if (!init) {
            deletePreloaders();
        }
    }

    var visibleBlockIndex = 0;

    function resetTest(options) {
        var $block = $(options.selector).slice(0, options.blocksNumber);

        $block.each(function (blockIndex) {
            if (options.initHide.indexOf(blockIndex) !== -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });

        visibleBlockIndex = 0;
    }

    function initForms(options) {
        var $block = $(options.selector).slice(0, options.blocksNumber);

        resetForms(options, true);

        $block.find('form').each(function () {
            var $that = $(this);

            $that.on('blank', function () {
                $(this).find('button')
                    .addClass('btn-disabled')
                    .removeClass('btn-' + options.buttonColor);
            }).on('full', function () {
                var isFull = true;

                $(this).find('.form-field.form-necessarily').each(function () {
                    if (!$(this).hasClass('form-ok')) {
                        isFull = false;
                    }
                });

                if ($(this).find('.checkbox input').length && !$(this).find('.checkbox input').prop('checked')) {
                    isFull = false;
                    $(this).find('button')
                        .addClass('btn-disabled')
                        .removeClass('btn-' + options.buttonColor);
                }

                if (isFull) {
                    $(this).find('button')
                        .removeClass('btn-disabled')
                        .addClass('btn-' + options.buttonColor);
                }
            });
        });

        $block.each(function (blockIndex) {
            var $currentBlock = $(this);

            var $btn = $currentBlock.find('button.btn, a.btn');

            if ($btn.data('evented')) {
                return;
            }

            $btn.data('evented', true);

            $btn.on('click', function (event) {
                var $button = $(this);
                var button = this;

                if ($button.hasClass('js-download')) {
                    return true;
                }

                if ($button.hasClass('btn-disabled')) {
                    event.preventDefault();
                } else {
                    if (options.formProps[blockIndex].ajax) {

                        var action = $currentBlock.find('form').attr('action');

                        // if ajax request is needed
                        $.ajax({
                            method: 'post',
                            url: action + (options.formProps[blockIndex].additionalUrl || ''),
                            beforeSend: function () {

                                $currentBlock.find('.form-error').removeClass('form-error');
                                $currentBlock.find('.form-message').remove();

                                if (!options.preloaderPlace) {
                                    $currentBlock
                                        .addClass('preloader-block')
                                        .append(getPreloader());
                                } else {
                                    $(options.preloaderPlace)
                                        .addClass('preloader-block')
                                        .append(getPreloader());
                                }
                            },
                            data: $currentBlock.find('form').serialize()
                        }).done(function (data) {

                            if (data.success) {
                                $currentBlock
                                    .removeClass('preloader-block')
                                    .hide()
                                    .next()
                                    .show().find('input').first().focus();

                                $('form.js-form-step-2').find('.js-hidden-field').remove();

                                if (action.indexOf('step/1') !== -1) {
                                    var inputs = ['first_name', 'last_name', 'email', 'phone'];
                                    if (typeof ga !== 'undefined') {
                                        ga('send', 'event', 'Forma', 'send', 'Registerstep1');
                                    }
                                    for (var i = 0; i < inputs.length; i++) {
                                        var inputName = inputs[i];
                                        ($('<input type="hidden" class="js-hidden-field" name="' + inputName + '"/>').val($currentBlock.find('[name="' + inputName + '"]').val().trim())).appendTo($('form.js-form-step-2'));
                                    }
                                }
                                if (action.indexOf('step/2') !== -1) {
                                    if (typeof ga !== 'undefined') {
                                        ga('send', 'event', 'Forma', 'send', 'Registerstep2');
                                    }
                                }
                                if ($block.find('.js-success-message').length) {
                                    $block.show();
                                    $block.find('form').hide();
                                    $block.find('.js-success-message').show();
                                    $block.find('.preloader-block__inner').hide();
                                }


                                button.dispatchEvent(new Event('hidePreloader'));
                            }
                            else {
                                $currentBlock.removeClass('preloader-block');
                                $currentBlock.find('.preloader-block__inner').remove();

                                var errorField = data.error_field;
                                var $input = $currentBlock.find('input[name="' + errorField + '"]').parents('.form-field');
                                if ($input.length > 0) {
                                    $input.addClass('form-error');
                                    $input.find('.form-message, .form-text').remove();
                                    $input.append('<span class="form-message">' + data.error_text + '</span>');
                                }
                            }
                        });
                    } else {
                        // if there is no necessary ajax request
                        if (blockIndex === options.blocksNumber - 1) {
                            $block.each(function (blockIndex) {
                                if (options.completeHide.indexOf(blockIndex) !== -1) {
                                    $(this).hide();
                                } else {
                                    $(this).show();
                                }
                            });

                        } else {
                            $currentBlock
                                .hide()
                                .next()
                                .show();
                        }
                        if (options.callback) {
                            options.callback();
                        }
                    }
                }
                event.preventDefault();
            })
        });
    }

    function initTest(options) {
        var $block = $(options.selector).slice(0, options.blocksNumber);

        var $submit = $('.popup-test__question_btn a');
        $block.each(function (blockIndex) {
            if (options.initHide.indexOf(blockIndex) !== -1) {
                $(this).hide();
            } else {
                $(this).show();
                updateSubmitState();
            }
        });


        function updateSubmitState() {
            var disabled = $('.popup-test__question_item:visible input[type=radio]:checked').length === 0;
            $submit.toggleClass('btn-disabled', disabled);
            $submit.toggleClass('btn-transp', !disabled);
        }

        $('.popup-test__question_item').each(function (index) {
            $(this).find('input[type=radio]').each(function (radioIndex) {
                $(this).on('change', function () {
                    updateSubmitState();
                });
            });
        });

        $('.popup-test__question_btn a').on('click', function (event) {
            if ($(this).hasClass('btn-disabled')) {
                return false;
            }
            if (visibleBlockIndex !== 3) {
                $($block[visibleBlockIndex++]).hide();
                $($block[visibleBlockIndex]).show();
                updateSubmitState();
            } else {
                $.ajax({
                    url: '#',
                    beforeSend: function () {
                        $($block[visibleBlockIndex])
                            .closest('.popup-inner')
                            .addClass('preloader-block')
                            .append(getPreloader());
                    },
                    data: []
                }).done(function (data) {
                    setTimeout(function () {
                        $($block[visibleBlockIndex])
                            .closest('.popup-inner')
                            .removeClass('preloader-block')
                        /*$($block[visibleBlockIndex + 1])
                         .hide(); */

                        var result = {};
                        $('.popup-test__question_item').each(function (index) {
                            $(this).find('input[type=radio]').each(function (radioIndex) {
                                if ($(this).is(':checked')) {
                                    result[index] = radioIndex;
                                }
                            });
                        });

                        var marks = [
                            [{rts: 1, nyse: 2}, {rts: 2, cme: 1}, {nyse: 2, cme: 1}],
                            [{cme: 2}, {rts: 1, cme: 2}, {nyse: 2, rts: 1}, {nyse: 1, rts: 1, cme: 2}],
                            [{rts: 2}, {nyse: 2}, {nyse: 1, cme: 1, rts: 1}],
                            [{rts: 2}, {nyse: 2}, {cme: 2}, {nyse: 1, cme: 1, rts: 1}]
                        ];

                        var votes = {nyse: 0, cme: 0, rts: 0};

                        for (var i = 0; i < marks.length; i++) {
                            var stepMark = result[i] in marks[i] ? marks[i][result[i]] : null;
                            if (stepMark) {
                                votes.nyse += 'nyse' in stepMark ? stepMark.nyse : 0;
                                votes.cme += 'cme' in stepMark ? stepMark.cme : 0;
                                votes.rts += 'rts' in stepMark ? stepMark.rts : 0;
                            }
                        }

                        var winner = null;
                        if ((votes.nyse >= votes.cme && votes.nyse >= votes.rts)) {
                            winner = 'nyse';
                        }
                        if ((votes.cme >= votes.nyse && votes.cme >= votes.rts)) {
                            winner = 'cme';
                        }
                        if ((votes.rts >= votes.nyse && votes.rts >= votes.cme)) {
                            winner = 'rts';
                        }

                        Popups.openById('thanks-' + winner);
                        visibleBlockIndex = 0;
                    }, 1000);
                });
            }
            event.preventDefault();
        });
    }

    function initMarket() {
        var $block = $('.markets-block');
        var $item = $block.find('.markets-block__item_inner');
        var thisIndex;

        $item.on('click', function () {
            thisIndex = $(this).parent().index();
        });

        Popups.addListener('market-popup', function ($popup) {
            $popup.find('.popup-market__inner').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1
            });

            $popup.find('.popup-market__inner').slick('slickGoTo', thisIndex);
        });
    }

    function initThanksPopupButtons() {
        $('.popup-thanks__btn a').on('click', function (event) {
            Popups.hide();
            event.preventDefault();
        })
    }

    function initPopups() {
        $('a[data-popup]').each(function () {
            var $button = $(this);
            switch ($button.data('popup')) {
                case 'reg':
                    $button.on('click', function () {
                        initForms({
                            selector: '.js-popup-reg .popup-inner',
                            buttonColor: 'red',
                            blocksNumber: 3,
                            formProps: [
                                {
                                    ajax: true,
                                    additionalUrl: ''
                                },
                                {
                                    ajax: true,
                                    additionalUrl: ''
                                },
                                {
                                    ajax: false
                                }
                            ],
                            initHide: [1, 2],
                            completeHide: [0, 1]
                        });
                    });
                    break;
                case 'team':
                    $button.on('click', function () {
                        initForms({
                            selector: '.js-popup-team > div',
                            buttonColor: 'red',
                            blocksNumber: 3,
                            formProps: [
                                {
                                    ajax: true
                                },
                                {
                                    ajax: true
                                },
                                {
                                    ajax: false
                                }
                            ],
                            initHide: [1, 2],
                            completeHide: [0, 1]
                        });
                    });
                    break;
                case 'new-stage':
                    $button.on('click', function () {
                        initForms({
                            selector: '.js-popup-new-stage > div',
                            buttonColor: 'red',
                            blocksNumber: 3,
                            formProps: [
                                {
                                    ajax: true
                                },
                                {
                                    ajax: true
                                },
                                {
                                    ajax: false
                                }
                            ],
                            initHide: [1, 2],
                            completeHide: [0, 1]
                        });
                    });
                    break;
                case 'trade':
                    $button.on('click', function () {
                        initForms({
                            selector: '.js-popup-trade > div',
                            buttonColor: 'red',
                            blocksNumber: 3,
                            formProps: [
                                {
                                    ajax: true
                                },
                                {
                                    ajax: true
                                },
                                {
                                    ajax: false
                                }
                            ],
                            initHide: [1, 2],
                            completeHide: [0, 1]
                        });
                    });
                    break;
                case 'test':
                    $button.on('click', function () {
                        deletePreloaders();
                        resetForms({
                            selector: '.popup-test > div',
                            buttonColor: 'red',
                            blocksNumber: 2,
                            formProps: [
                                {
                                    ajax: false
                                },
                                {
                                    ajax: false
                                }
                            ],
                            initHide: [1],
                            completeHide: [0]
                        });

                        resetTest({
                            selector: '.popup-test__question > div',
                            buttonColor: 'red',
                            blocksNumber: 5,
                            initHide: [1, 2, 3],
                            completeHide: [0, 1, 2, 3, 4]
                        });

                        $('.popup-test__preview_btn .btn').trigger('click');
                    });
                    break;
            }
        });
        $('.js-close-wnd').on('click', function (event) {
            deletePreloaders();
        })
    }

    $(function () {

        initForms({
            selector: '.js-support-form',
            buttonColor: 'green',
            blocksNumber: 3,
            formProps: [
                {
                    ajax: true,
                    additionalUrl: ''
                },
                {
                    ajax: true,
                    additionalUrl: ''
                },
                {
                    ajax: false
                }
            ],
        });

        initForms({
            selector: '.game-block.js-game-reg',
            buttonColor: 'green',
            blocksNumber: 3,
            formProps: [
                {
                    ajax: true,
                    additionalUrl: ''
                },
                {
                    ajax: true,
                    additionalUrl: ''
                },
                {
                    ajax: false
                }
            ],
            initHide: [0, 1, 2],
            completeHide: [0, 1, 2],
            callback: function () {
                deletePreloaders();
                resetForms({
                    selector: '.game-block.js-game-reg',
                    buttonColor: 'green',
                    blocksNumber: 3,
                    initHide: [0, 1, 2]
                });
            }
        });
        initForms({
            selector: '.js-popup-reg .popup-inner',
            buttonColor: 'red',
            blocksNumber: 3,
            formProps: [
                {
                    ajax: true,
                    additionalUrl: ''
                },
                {
                    ajax: true,
                    additionalUrl: ''
                },
                {
                    ajax: false
                }
            ],
            initHide: [1, 2],
            completeHide: [0, 1]
        });

        initForms({
            selector: '.js-popup-new-stage > div',
            buttonColor: 'red',
            blocksNumber: 3,
            formProps: [
                {
                    ajax: true
                },
                {
                    ajax: true
                },
                {
                    ajax: false
                }
            ],
            initHide: [1, 2],
            completeHide: [0, 1]
        });
        initForms({
            selector: '.js-popup-team > div',
            buttonColor: 'red',
            blocksNumber: 3,
            formProps: [
                {
                    ajax: true
                },
                {
                    ajax: true,
                },
                {
                    ajax: false
                }
            ],
            initHide: [1, 2],
            completeHide: [0, 1]
        });
        initForms({
            selector: '.js-popup-trade > div',
            buttonColor: 'red',
            blocksNumber: 3,
            formProps: [
                {
                    ajax: true
                },
                {
                    ajax: true
                },
                {
                    ajax: false
                }
            ],
            initHide: [1, 2],
            completeHide: [0, 1]
        });

        initForms({
            selector: '.js-bottom-reg-form > div',
            buttonColor: 'red',
            blocksNumber: 4,
            formProps: [

                {
                    ajax: true
                },
                {
                    ajax: true
                },
                {
                    ajax: true
                }
            ],
            initHide: [2, 3],
            completeHide: [1, 2, 3],
            callback: function () {
                deletePreloaders();
                resetForms({
                    selector: '.js-bottom-reg-form > div',
                    buttonColor: 'red',
                    blocksNumber: 4,
                    initHide: [1, 2]
                });
            }
        });

        initForms({
            selector: '.popup-test> div',
            buttonColor: 'red',
            blocksNumber: 2,
            formProps: [
                {
                    ajax: false
                },
                {
                    ajax: false
                }
            ],
            initHide: [1],
            completeHide: [0]
        });

        initTest({
            selector: '.popup-test__question > div',
            buttonColor: 'red',
            blocksNumber: 5,
            initHide: [1, 2, 3],
            completeHide: [0]
        });

        initInput();

        initThanksPopupButtons();

        initPopups();

        $('.fancybox-media').fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                media: {},
                overlay: {
                    locked: false
                }
            }
        });

        Popups.Init();

        initMarket();
    });

    $(function () {
        $("#tabs, #subtabs, #tabs-table__nyse, #tabs-table__cme, #tabs-table__moex, #tabs-table__fx").tabs({
            collapsible: false
        });
        $(function () {
            $("#accordion, #accordion-nyse, #accordion-cme, #accordion-moex, #accordion-fx").accordion({
                heightStyle: "content"
            });
        });

    });
})(jQuery);